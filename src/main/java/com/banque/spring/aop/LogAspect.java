package com.banque.spring.aop;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

/**
 * Log tous les appels vers les services.
 */
@Aspect
public class LogAspect {
	
	private final static Log LOG = LogFactory.getLog(LogAspect.class);
	
	/**
	 * Constructeur de l'objet.
	 */
	public LogAspect() {
		super();
	}
	
	/**
	 * Executer avant l'appel à un service
	 * @param jp
	 */
	@Before("execution( * com.banque.service.*Service.*(..))")
	public void logBefore(JoinPoint jp) {
		 if (LogAspect.LOG.isInfoEnabled()) {
				LogAspect.LOG.info("Passage avant " + jp.getTarget() + " " + jp.getSignature());
			}
	}	
	
	/**
	 * Executer après l'appel à un service
	 * @param jp
	 */
	@After("execution( * com.banque.service.*Service.*(..))")
	public void logAfter(JoinPoint jp) {
		if(LogAspect.LOG.isInfoEnabled()) {
			LogAspect.LOG.info("Passage apres " + jp.getTarget() + " " + jp.getSignature());
		}
	}

}
