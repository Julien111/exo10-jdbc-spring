/**
 * Copyright :     <br/>
 *
 * @version 1.0<br/>
 */
package com.banque.dao;

import java.io.Serializable;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import com.banque.dao.ex.ExceptionDao;
import com.banque.dao.util.AbstractJdbcMapper;
import com.banque.entity.IEntity;

/**
 * DAO standard.
 *
 * @param <T>
 *            la cible du DAO
 */
@Repository
public abstract class AbstractDAO<T extends IEntity> implements Serializable,
IDAO<T> {

	private static final long serialVersionUID = 1L;
		
	private JdbcTemplate jdbcTemplate;	

	protected Log LOG = LogFactory.getLog(this.getClass());	
	
	/**
	 * Constructeur vide.
	 */
	public AbstractDAO() {
		super();		
	}

	@Override
	public abstract String getTableName();

	@Override
	public abstract String getPkName();

	@Override
	public abstract String getAllColumnNames();

	@Override
	public abstract T insert(T uneEntite)
			throws ExceptionDao;

	@Override
	public abstract T update(T uneEntite)
			throws ExceptionDao;

	@Override
	public boolean delete(T pUneEntite) throws ExceptionDao {
	if (pUneEntite == null) {
		return false;
	}
	if (pUneEntite.getId() == null) {
		throw new ExceptionDao("L'entite n'a pas d'ID");
	}
	try {
		return this.getJdbcTemplate().update("delete from " + this.getTableName() + " where " + this.getPkName() + "=?;",
		    pUneEntite.getId()) > 0;
	} catch (DataAccessException e) {
		throw new ExceptionDao(e);
	}	
	}
	
	@Override
    public T select(Object pUneClef) throws ExceptionDao {
        if (pUneClef == null) {
            return null;
        } 
        T result = null;
 
        try {
            result = this.getJdbcTemplate().queryForObject(
                    "select " + this.getAllColumnNames() + " from "
                            + this.getTableName() + " where "
                            + this.getPkName() + " = ?",
                     this.getMapper(),new Object[] { pUneClef });
        } catch (Exception e) {
            throw new ExceptionDao(e);
        }
        return result;
    }


	@Override
	public List<T> selectAll(String pAWhere, String pAnOrderBy) throws ExceptionDao {				
				
		List<T> result = new ArrayList<>();
		
		try {

			StringBuffer request = new StringBuffer();
			request.append("select ").append(this.getAllColumnNames())
			.append(" from ");
			request.append(this.getTableName());
			if (pAWhere != null) {
				request.append(" where ");
				request.append(pAWhere);
			}
			if (pAnOrderBy != null) {
				request.append(" order by ");
				request.append(pAnOrderBy);
			}
			request.append(';');
			if (this.LOG.isDebugEnabled()) {
				this.LOG.debug("Requete: " + request.toString());
			}
		    result = this.jdbcTemplate.query(request.toString(), (ResultSetExtractor<List<T>>)this.getMapper());		

		} catch (Exception e) {
            throw new ExceptionDao(e);
        }
		return result;
	}	
	
	/**
	 * new method
	 * @return
	 */
	protected abstract AbstractJdbcMapper<T> getMapper();

	/**
	 * Place les elements dans la requete.
	 *
	 * @param ps
	 *            la requete
	 * @param gaps
	 *            les elements
	 * @throws SQLException
	 *             si un des elements ne rentre pas
	 */
	protected void setPrepareStatement(PreparedStatement ps, List<Object> gaps)
			throws SQLException {
		Iterator<Object> iter = gaps.iterator();
		int id = 0;
		while (iter.hasNext()) {
			id++;
			Object lE = iter.next();
			if (lE == null) {
				continue;
			}
			if (lE instanceof String) {
				ps.setString(id, (String) lE);
			} else if (lE instanceof Date) {
				ps.setDate(id, (Date) lE);
			} else if (lE instanceof java.util.Date) {
				ps.setDate(id, new Date(((java.util.Date) lE).getTime()));
			} else if (lE instanceof Timestamp) {
				ps.setTimestamp(id, (Timestamp) lE);
			} else if (lE instanceof Integer) {
				ps.setInt(id, ((Integer) lE).intValue());
			} else if (lE instanceof Double) {
				ps.setDouble(id, ((Double) lE).doubleValue());
			} else {
				throw new SQLException("Invalid type '"
						+ lE.getClass().getSimpleName() + "'");
			}

		}
	}	
			
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
	
	/**
	 * Charger la propriété jdbc dans le bean
	 * @param pJdbcTemplate
	 */
	@Autowired
	public void setJdbcTemplate(@Qualifier("jdbcTemplate") JdbcTemplate pJdbcTemplate) {
		this.jdbcTemplate = pJdbcTemplate;
	}
	
}
