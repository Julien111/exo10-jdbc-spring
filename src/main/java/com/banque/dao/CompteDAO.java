/**
 * Copyright :     <br/>
 *
 * @version 1.0<br/>
 */
package com.banque.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.banque.dao.ex.ExceptionDao;
import com.banque.dao.util.AbstractJdbcMapper;
import com.banque.dao.util.CompteJdbcMapper;
import com.banque.entity.ICompteEntity;

/**
 * Gestion des comptes.
 */
@Repository
public class CompteDAO extends AbstractDAO<ICompteEntity> implements ICompteDAO {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructeur vide
	 */	
	public CompteDAO() {
		
	}
	
	@Override
	public String getTableName() {
		return "compte";
	}

	@Override
	public String getPkName() {
		return "id";
	}

	@Override
	public String getAllColumnNames() {
		return "id,libelle,solde,decouvert,taux,utilisateurId";
	}

	@Override
	public ICompteEntity insert(final ICompteEntity pUneEntite) throws ExceptionDao {
	if (pUneEntite == null) {
		return null;
	}
	try {
		PreparedStatementCreator psc = new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connexion) throws SQLException {
				PreparedStatement ps = connexion.prepareStatement("insert into " + CompteDAO.this.getTableName()
				    + " (libelle, solde, decouvert, taux, utilisateurId) values (?,?,?,?,?);", Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, pUneEntite.getLibelle());
				ps.setDouble(2, pUneEntite.getSolde());
				ps.setDouble(3, pUneEntite.getDecouvert());
				ps.setDouble(4, pUneEntite.getTaux());
				ps.setDouble(5, pUneEntite.getUtilisateurId().intValue());	
				 
				return ps;
			}
		};
		KeyHolder kh = new GeneratedKeyHolder();
		
		this.getJdbcTemplate().update(psc, kh);
		pUneEntite.setId(Integer.valueOf(kh.getKey().intValue()));
	} catch (Throwable e) {
		throw new ExceptionDao(e);
	}

	return pUneEntite;
	}

	@Override
	public ICompteEntity update(final ICompteEntity pUneEntite)
			throws ExceptionDao {

		if (pUneEntite == null) {
			return null;
		}
		if(pUneEntite.getId() == null) {
			throw new ExceptionDao("L'entite n'a pas d'ID");
		}
		try {	
			
			String id = CompteDAO.this.getPkName();	
			
			this.getJdbcTemplate().update("update " + CompteDAO.this.getTableName()
		    +" set libelle=?, solde=?, decouvert=?, taux=?, utilisateurId=? where " 
			+ id + "=?;", pUneEntite.getLibelle(), pUneEntite.getSolde().doubleValue(),
			pUneEntite.getDecouvert().doubleValue(), pUneEntite.getTaux().doubleValue(),
			pUneEntite.getUtilisateurId().intValue(), pUneEntite.getId().intValue());
			    
		}
		catch(Exception e) {
			throw new ExceptionDao(e);
		}
		return pUneEntite;
	}

	@Override
	protected AbstractJdbcMapper<ICompteEntity> getMapper() {		
		return  new CompteJdbcMapper();
	}

}